package encoder;

import exceptions.BackendException;

abstract class Encoder {
    abstract byte[] encode() throws BackendException;
}
